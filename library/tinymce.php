<?php
// These filters are from the Plugin Name: Visual Code Editor - http://www.webveteran.com/blog/index.php/visual-code-editor/

// Modify Tiny_MCE init
add_filter('tiny_mce_before_init', 'smartenTinyMCE' );
function smartenTinyMCE($init) {
	// Add PRE and CODE to the block formats menu
	$init['theme_advanced_blockformats'] = 'h2,h3,h4,h5,h6,p,blockquote,pre,code';
		
	// Allow extra attributes for some syntax highlighters, IE: <pre lang="php" line="5">...</pre)
	// Allow iFrames for live examples
	$init['extended_valid_elements'] = 'pre[*],code[*],iframe[*]';

	return $init;
}

// Unescape WP's double escaping of '&'
add_filter('the_editor_content', 'fixAmps');
function fixAmps($post_content = '') {
	return preg_replace("/&amp;amp/i", "&amp", $post_content);
}

// Remove extra PRE tag around [sourcecode] blocks in posts
add_filter('the_content', 'removePrePost');
function removePrePost($post_content = '') {
	$preStart = '<pre>\n<pre class="syntax-highlight:';
	$preEnd = addcslashes('</pre>\n</pre>', '/');
	$post_content = preg_replace("/$preStart/i", '<pre class="syntax-highlight:', $post_content);
	return preg_replace("/$preEnd/i", '</pre>', $post_content);
}

// Remove extra PRE tag around [sourcecode] blocks in comments
add_filter('comment_text', 'removePreComment');
function removePreComment($comment = '') {
	$preStart = addcslashes('<pre><pre class="syntax-highlight:', '/');
	$preEnd = addcslashes('</pre>\n</pre>', '/');
	$comment = preg_replace("/$preStart/i", '<pre class="syntax-highlight:', $comment);
	return preg_replace("/$preEnd/i", '</pre>', $comment);
}

//************************* my mods ****************************

// Add styles to the styles menu (instead of the ones in editor-style.css)
// REF: http://alisothegeek.com/2011/05/tinymce-styles-dropdown-wordpress-visual-editor/
add_filter( 'tiny_mce_before_init', function ( $settings ) {

    $style_formats = array(
    	array(
    		'title' => 'code (inline)',
    		'inline' => 'code',
    	),
        array(
        	'title' => 'PullQuote',
        	'block' => 'div',
        	'classes' => 'pullquote',
        	'wrapper' => true
        ),
        array(
        	'title' => 'Bold Red Text',
        	'inline' => 'span',
        	'styles' => array(
        		'color' => '#f00',
        		'fontWeight' => 'bold'
        	)
        )
    );

    $settings['style_formats'] = json_encode( $style_formats );

    return $settings;

} );






// This sets the Visual Editor as default #
//add_filter( 'wp_default_editor', create_function('', 'return "tinymce";') );
// This sets the HTML Editor as default #
//add_filter( 'wp_default_editor', create_function('', 'return "html";') );  

// customize the plugins - has problems that need working out
add_filter('mce_external_plugins', 'customizeTinyMCEPlugins');
function customizeTinyMCEPlugins($plugins)
{
// existing 		$plugins = array( 'inlinepopups', 'spellchecker', 'tabfocus', 'paste', 'media', 'wordpress', 'wpfullscreen', 'wpeditimage', 'wpgallery', 'wplink', 'wpdialogs' );
  
  $addIn = array(
//    'style',            // btn: styleselect
//    'searchreplace' => get_stylesheet_directory_uri() . '/tinymce/searchreplace/editor_plugin.js',    // btn: search, replace
//    'table',            // btn: table
//    'insertdatetime',   // btn: insertdate, inserttime
//    'visualchars',      // btn: visualchars
//    'nonbreaking',      // btn: nonbreaking
//    'contextmenu'       // modifies right-btn menu
);
  $plugins = array_merge($plugins, $addIn);

  return $plugins;
}

// customize toolbar row 1
add_filter('mce_buttons', 'customizeTinyMCEToolbar1');
function customizeTinyMCEToolbar1($row)
{
  // throw away default row
  $row = array('bold', 'italic', 'underline', 'strikethrough', '|', 'bullist', 'numlist', 'blockquote', '|', 'justifyleft', 'justifycenter', 'justifyright', 'justifyfull', '|', 'forecolor', 'backcolor', '|', 'link', 'unlink', 'anchor', 'wp_more', '|', 'spellchecker', 'fullscreen', 'wp_adv', 'wp_help');
  return $row;
}

// customize toolbar row 2
add_filter('mce_buttons_2', 'customizeTinyMCEToolbar2');
function customizeTinyMCEToolbar2($row)
{
  // throw away default row
// removed for now: 'styleselect', 'fontselect', 'fontsizeselect', 
  $row = array('formatselect', 'cut', 'copy', 'paste', 'pastetext', 'pasteword', '|', 'removeformat', 'hr', 'charmap', '|', 'outdent', 'indent', '|', 'undo', 'redo', '|', 'search', 'replace', '|' , 'cleanup', 'code', '|', 'tablecontrols');
//'sub', 'sup', 'hr', 
  return $row;
}

// customize toolbar row 3
//add_filter('mce_buttons_3', 'customizeTinyMCEToolbar3');
/*function customizeTinyMCEToolbar3($row)
{
  // throw away default row
  $row = array('tablecontrols', '|' ,,, '|', 'insertdate', 'inserttime', '|', 'visualchars', 'visualaid', 'nonbreaking');
  return $row;
}
*/


//add_filter('teeny_mce_before_init', 'customizeTinyMCE');
/*
To edit final code, do something like this:

function customizeTinyMCE($initArray)
{
// these are from what I did to ZenCart & Symfony - SiteBuilder:
//  theme_advanced_buttons1 : "cut,copy,paste,pastetext,pasteword,|,undo,redo,|,search,replace,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,forecolor,backcolor,|,spellchecker,fullscreen,code,help",
//  theme_advanced_buttons2 : "styleselect,formatselect,fontselect,fontsizeselect,|,bullist,numlist,|,outdent,indent,|,link,unlink,anchor,image",
//  theme_advanced_buttons3 : "tablecontrols,|,sub,sup,nonbreaking,blockquote,hr,charmap,|,insertdate,inserttime,|,visualchars,visualaid,removeformat,cleanup,preview",
//  plugins : "paste,save, inlinepopups,spellchecker,fullscreen,contextmenu,searchreplace,table,style,insertdatetime,preview,visualchars,nonbreaking",



  $row1 = array('bold', 'italic', 'underline', 'strikethrough', '|', 'bullist', 'numlist', 'blockquote', '|', 'justifyleft', 'justifycenter', 'justifyright', 'justifyfull', '|', 'forecolor', 'backcolor', '|', 'link', 'unlink', 'anchor', 'wp_more', '|', 'spellchecker', 'fullscreen', 'wp_adv');
  $row2 = array('styleselect', 'formatselect', 'fontselect', 'fontsizeselect', 'cut', 'copy', 'paste', 'pastetext', 'pasteword', 'removeformat', '|', 'charmap', '|', 'outdent', 'indent', '|', 'undo', 'redo', '|', 'search', 'replace', '|' , 'code', 'wp_help');
  $row3 = array('tablecontrols', '|' ,' sub', 'sup', 'nonbreaking','hr', 'charmap', '|', 'insertdate', 'inserttime', '|', 'visualchars', 'visualaid', 'removeformat' , 'cleanup');

	$initArray['theme_advanced_buttons2'] => $row1;
	$initArray['theme_advanced_buttons3'] => $row2;
	$initArray['theme_advanced_buttons4'] => $row3;
}
*/

