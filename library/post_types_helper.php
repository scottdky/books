<?php

//---------------------------------------------------------------------------
// *********** Handle Ajax Posts ********************************************
//---------------------------------------------------------------------------

// this code handles inline quick editing & bulk editing
//ini_set('display_errors', 1);
//error_reporting(E_ALL);

if(isset($_POST['edit_action'])){
  $wp_dir = realpath(__DIR__.'/../../../../');

  // load required wordpress files
  require_once($wp_dir.'/wp-load.php');

 // require_once('./includes/admin.php');

  // handle quick (inline) edit - basically get the correct values to populate the form
	if($_POST['edit_action'] == 'ajaxget'){
		$post_id = $_POST['post_id'];
    $meta_key = $_POST['meta_key'];
		echo get_post_meta($post_id, $meta_key, true);
	}
	elseif($_POST['edit_action'] == 'ajaxsave') {
		$post_ids = explode(" ", $_POST['post_ids']);
    $meta_key = $_POST['meta_key'];
    $meta_val = $_POST['meta_val'];
		foreach ($post_ids as $post_id)
			update_post_meta($post_id, $meta_key, $meta_val);
		echo "Event date updated.";
	}
	return; // this is not a function - we want to force a return on ajax calls
}


// Helper funcs for post types & taxonomies

//---------------------------------------------------------------------------
// *********** 1: Create the Custom Post Type *******************************
//---------------------------------------------------------------------------

// REFS:
// http://thinkvitamin.com/code/create-your-first-wordpress-custom-post-type/

// Implement according to this example:
/**
    add_action( 'init', function () {
      register_post_type( POST_TYPE_book_contents,
        array(
          'labels' => array(
            'name' => __('Contents'),
            'singular_name' => __('Content'),
          ),
          'description' => __('Contains contents of books - sections, chapters...'),
          'public' => true,
          'menu_position' => 6,
          //'menu_icon' => get_stylesheet_directory_uri() . '/images/super-duper.png',
          'hierarchical' => true,
          'rewrite' => array( 'slug' => 'books_contents', 'with_front' => true ),
          //'taxonomies' => array( 'post_tag', 'category', 'section'),
          'supports' => array('title','editor', 'page-attributes', 'author', 'comments', 'revisions', 'post-formats')
        )
      );
    } );
*/


//---------------------------------------------------------------------------
// *********** 2: Create the Taxonomies *************************************
//---------------------------------------------------------------------------

// Implement according to this example:
/**
    add_action( 'init', function () {

      register_taxonomy(TAX_SLUG_section_type, 
        array(POST_TYPE_book_contents),  // apply to only this post_type
        array(
        'hierarchical' => false,
        'label' => __('Types'),
        'singular_label' => __('Type'),
        'show_ui' => true,
        'query_var' => true,
        'rewrite' => true, //array( 'slug' => 'section' ),
      ));

      // register a 2nd one
      register_taxonomy(TAX_SLUG_book_ref, 
        ....
      ));

    }, 0);
*/

//---------------------------------------------------------------------------
// *********** 3: Create the NEW List Post Columns **************************
//---------------------------------------------------------------------------

// REFS:
// http://nspeaks.com/1015/add-custom-taxonomy-columns-to-edit-posts-page/
// http://thinkvitamin.com/code/create-your-first-wordpress-custom-post-type/


// 3a: Define the extra columns and where they go

// Invoke as follows:
//      add_filter( 'manage_edit-'.POST_TYPE_book_contents.'_columns', function ($defaults) {
//        $newcols = array(); // create new columns array
//        $newcols['book_ref'] = __('Book');
//        $newcols['section_type'] = __('Type');
//        return insert_new_cols($defaults, 2, $newcols);
//      } );

function insert_new_cols($defaults, $col_place, $new_cols) {
  $cols = array(); // create new columns array

  // loop thru all columns, copying them, and inserting our in the order we want
  $index = 0;
  foreach($defaults as $key=>$val) {
    if ($index == $col_place) {
      foreach($new_cols as $newkey=>$newval) {
        $cols[$newkey] = $newval;
      }
    }
    $cols[$key] = $val;
    $index++;
  }
  return $cols;
}

// 3b: provide html & values to display each of our custom columns

// -- must use manage_page_custom_column for hierarchical post types, and manage_post_custom_column for non-hierarchical types
// - see http://codex.wordpress.org/Plugin_API/Action_Reference/manage_posts_custom_column

// Implement according to this example:
/**
add_action('manage_pages_custom_column', function ($column, $post_id) {
  global $wpdb;
  global $gBookContentOptions;

  switch ($column) {
      case "book_ref":
        echo get_the_term_list($post_id, 'book_ref', '', ', ','');
        break;
      case "section_type":
        echo get_the_term_list($post_id, 'section_type', '', ', ','');
        break;
      case "progress":
//        $progStr = get_post_meta($post_id, 'progress', true);
//        echo ($progStr === '')? 'Unstarted' : $meta_values[$progStr];
        echo get_the_term_list($post_id, 'progress', '', ', ','');
        break;
      case "order":
        echo get_post($post_id)->menu_order;
        break;
      case 'password':
        echo get_post($post_id)->post_password;
        break;
//      case "section_slug":
//        echo $custom["section_slug"][0];
//        break;
  }
} , 10, 2);
*/


//---------------------------------------------------------------------------
// *********** 4: Create the NEW Post Listing Filters **********************
//---------------------------------------------------------------------------

// REF - from Charles Alexandre (near the bottom) - http://wordpress.stackexchange.com/questions/578/adding-a-taxonomy-filter-to-admin-list-for-a-custom-post-type

// Step 4a: define the filter combo-boxes (drop-down for the sections column for filtering posts)

// for taxonomies, invoke like this:
//   add_action('restrict_manage_posts', function () {
//     create_taxonomy_filter_dropdown(CUSTOM_POST_TYPE, TAXONOMY_SLUG, array('hide_empty' => false));
//   } );

function create_taxonomy_filter_dropdown($post_type, $tax_slug, $dropdown_args = array()) {
  global $typenow;
  if ($typenow == $post_type) {
    $selected = isset($_GET[$tax_slug]) ? $_GET[$tax_slug] : '';
    $tax_obj = get_taxonomy($tax_slug);

    $args = $dropdown_args;
    $args['show_option_all'] = isset($args['show_option_all'])? $args['show_option_all'] : __("Show All {$tax_obj->label}");
    $args['taxonomy'] = $tax_slug;
    $args['name'] = $tax_slug;
    $args['orderby'] = isset($args['orderby'])? $args['orderby'] : 'name';
    $args['selected'] = isset($args['selected'])? $args['selected'] : $selected;
    $args['show_count'] = isset($args['show_count'])? $args['show_count'] : true;
    $args['hide_empty'] = isset($args['hide_empty'])? $args['hide_empty'] : true;

    wp_dropdown_categories($args);
  }
}

// for custom fields, invoke like this:
//   add_action('restrict_manage_posts', function () {
//     create_custom_field_dropdown(CUSTOM_POST_TYPE, META_KEY, $meta_values);
//   } );
//    $meta_values = array('option1', 'option2' ...)

function create_custom_field_dropdown($post_type, $meta_key, $meta_values, $show_all = '') {
  global $wpdb;
  global $typenow;

  if ($typenow == $post_type) {
    $selected = isset($_GET[$meta_key]) ? $_GET[$meta_key] : '';
    $options = $meta_values;

    // get a count for each time an option is used
    $counts = array();
    $table_name = $wpdb->postmeta;
    foreach ($options as $key => $option) {
      $sql = "SELECT COUNT(*) FROM $wpdb->postmeta WHERE meta_key = %s AND meta_value = %s";
      // statement is from http://codex.wordpress.org/Class_Reference/wpdb
      $counts[$key] = $wpdb->get_var( $wpdb->prepare( $sql, $meta_key, $key ) );
    }

    ?>
    <select name="<?php echo $meta_key ?>">
    <?php if (!empty($show_all)): ?>
    <option value=""><?php echo $show_all; ?></option>
    <?php endif; ?>
    <?php foreach ($options as $key => $option): ?>
      <option <?php echo ($selected !== '' && $key==$selected)? 'selected="selected"' : '' ?> value="<?php echo $key ?>"><?php echo $option . '(' . $counts[$key] . ')' ?></option>
    <?php endforeach; ?>
    </select> 
    <?php

  }
}

// for author column, invoke like this:
//   add_action('restrict_manage_posts', function () {
//     create_custom_field_dropdown(CUSTOM_POST_TYPE, META_KEY, $meta_values);
//   } );
//    $meta_values = array('option1', 'option2' ...)

function create_author_dropdown($post_type) {
  global $typenow;
  if ($typenow == $post_type) {
    $selected = isset($_GET['author']) ? $_GET['author'] : 0;

    wp_dropdown_users(array(
            'show_option_all'       => 'Show all Authors',
            'show_option_none'      => false,
            'name'                  => 'author',
            'selected'              => $selected,
            'include_selected'      => false
    ));
  }
}

// Step 4b: Define the actual filters funcs

// Filter Taxonomy Columns; works for all taxonomies
// this function adds a wp filter for changing the results from id to slug for all taxonomies
// REF - from Drew Gourley - http://wordpress.stackexchange.com/questions/578/adding-a-taxonomy-filter-to-admin-list-for-a-custom-post-type

// Invoke as follows:
//      add_filter('parse_query', function ($query) {
//        taxonomy_filter($query, POST_TYPE);
//      } );

function taxonomy_filter($query, $post_type) {
    global $typenow;
    global $pagenow;
    if (($typenow == $post_type) && ($pagenow=='edit.php')) {
        $filters = get_object_taxonomies($typenow);
        foreach ($filters as $tax_slug) {
            $var = &$query->query_vars[$tax_slug];
            if ( isset($var) ) {
                $term = get_term_by('id',$var,$tax_slug);
                if (is_object($term)) {
                  $var = $term->slug;
                  //echo "slug: {$term->slug}; id: $var";
                }
            }
        }
    }
}


// Filter for Custom Fields
// REF: http://wordpress.stackexchange.com/questions/16637/how-to-filter-post-listing-in-wp-dashboard-posts-listing-using-a-custom-field/16641#16641

// Invoke as follows:
//      add_filter('parse_query', function ($query) {
//        custom_field_filter($query, $post_type, $meta_key);
//      } );

function custom_field_filter($query, $post_type, $meta_key) {
  global $typenow;
  global $pagenow;
  if ( is_admin() && ($typenow == $post_type) && ($pagenow=='edit.php') && (isset($_GET[$meta_key]) && $_GET[$meta_key] != '')) {
// query filter changed in 3.1, so this no longer works:
      //$query->query_vars['meta_key'] = $meta_key;
      //$query->query_vars['meta_value'] = $_GET[$meta_key];
// use this form instead: - REF: http://wordpress.org/support/topic/parse_query-filter-changed-in-31
    set_query_var( 'meta_query', array( array( 'key' => $meta_key, 'value' => $_GET[$meta_key] ) ) );
    set_query_var( 'orderby','meta_value' );
  }
}


//---------------------------------------------------------------------------
// *********** 5: Create custom data types & widgets for edit post page *****
//---------------------------------------------------------------------------
// REFS:
// http://thinkvitamin.com/code/create-your-first-wordpress-custom-post-type/
// for drop-down boxes for taxonomies - http://wordpress.stackexchange.com/questions/28290/custom-taxonomy-as-dropdown-in-admin

// 5a: Create the widgets (meta-bax), with their respective html def funcs

// Implement as follows:

//      add_action('add_meta_boxes', function () {
//        remove_meta_box('tagsdiv-'.TAX_SLUG_book_ref, POST_TYPE_book_contents, 'side' );
        // meaning: add_meta_box( $id, $title, $callback, $post_type, $context, $priority )
//        add_meta_box('tagsdiv-'.TAX_SLUG_book_ref, __('Book'), 
//          function ($post) {  create_taxonomy_meta_box($post, TAX_SLUG_book_ref, __('Select which book the post belongs to')); }, 
//          POST_TYPE_book_contents, 'side', 'default');
//        add_meta_box(...); // add a 2nd
//      } );


/* Prints the taxonomy box content */
function create_taxonomy_meta_box($post, $tax_slug, $help_text = 'Select your type') {
  ?>
  <div class="tagsdiv" id="<?php echo $tax_slug; ?>">
    <div class="jaxtag">
    <?php 
    // Use nonce for verification
    wp_nonce_field( plugin_basename( __FILE__ ), $tax_slug.'_noncename' );
    $type_IDs = wp_get_object_terms( $post->ID, $tax_slug, array('fields' => 'ids') );
//    wp_dropdown_categories('taxonomy=types&hide_empty=0&orderby=name&name=types&show_option_none=Select type&selected='.$type_IDs[0]); 

    wp_dropdown_categories(array(
      'taxonomy' => $tax_slug,
      'name' => $tax_slug,
      'orderby' => 'name',
      'selected' => $type_IDs[0],
      'hide_empty' => false,
    ));

    ?>
    <p class="howto"><?php echo $help_text ?></p>
    </div>
  </div>
  <?php
}

// 5a: Create the widgets (meta-bax), with their respective html def funcs
function create_custom_field_input_meta_box($post, $meta_key, $label, $help_text = '', $divstyle = 'custom-widget') {
  global $post;

//  $custom = get_post_custom($post->ID);
//  $value = $custom[$meta_key][0];
  $value = get_post_meta($post->ID, $meta_key, true);
  if (is_array($value))
    $value = implode ( ', ', $value );
  ?>
    <div class="widget <?php echo $divstyle ?>">
    <label><strong><?php echo $label ?></strong></label><br/>
    <input name="<?php echo $meta_key ?>" value="<?php echo $value; ?>" /><br/>
    <span class="howto"><?php echo $help_text ?></span>
    </div>
  <?php
}

// 5a: Create the widgets (meta-bax), with their respective html def funcs
function create_custom_field_textarea_meta_box($post, $meta_key, $label, $help_text = '') {
  global $post;

  $custom = get_post_custom($post->ID);
  $value = $custom[$meta_key][0];
  ?>
    <div class="widget custom-widget area-widget">
    <label><strong><?php echo $label ?></strong></label>
    <textarea rows="24" style="width: 100%" name="<?php echo $meta_key ?>" ><?php echo $value; ?></textarea>
    <span class="howto"><?php echo $help_text ?></span>
    </div>
  <?php
}

function create_custom_field_select_meta_box($post, $meta_key, $meta_values, $label, $help_text = '') {
  global $post;

  $custom = get_post_custom($post->ID);
  $value = $custom[$meta_key][0];
  $options = $meta_values;
  ?>
  <div class = "widget custom-widget">
    <?php if (!empty($label)): ?>
    <label><strong><?php echo $label ?></strong></label>
    <?php endif; ?>
    <select name="<?php echo $meta_key ?>">
    <?php foreach ($options as $key => $option): ?>
      <option <?php echo ($key==$value)? 'selected="selected"' : '' ?> value="<?php echo $key ?>"><?php echo $option ?></option>
    <?php endforeach; ?>
    </select> 
    <p class="howto"><?php echo $help_text ?></p>
  </div>
  <?php
}



// 5b: Define the widget's save func

// Implement as follows:

//      add_action( 'save_post', function ( $post_id ) {
//        taxonomy_widget_save_func($post_id, TAX_SLUG_section_type);
//        taxonomy_widget_save_func(...); // save the 2nd one
//      } );

function taxonomy_widget_save_func($post_id, $tax_slug) {

  // verify if this is an auto save routine. 
  // If it is our form has not been submitted, so we dont want to do anything
  if ( ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) || wp_is_post_revision( $post_id ) ) 
      return;

  // verify this came from the our screen and with proper authorization,
  // because save_post can be triggered at other times
  if ( !wp_verify_nonce( $_POST[$tax_slug.'_noncename'], plugin_basename( __FILE__ ) ) )
      return;

  // Check permissions
  if (( POST_TYPE_book_contents != $_POST['post_type'] ) || ( !current_user_can( 'edit_page', $post_id ) ))
      return;

  // OK, we're authenticated: we need to find and save the data
  $type_ID = $_POST[$tax_slug];
  $type = ( $type_ID > 0 ) ? get_term( $type_ID, $tax_slug )->slug : NULL;
  wp_set_object_terms(  $post_id , $type, $tax_slug );

}

// Implement as follows:

//      add_action( 'save_post', function ( $post_id ) {
//        custom_field_meta_save_func($post_id, $meta_key);
//      } );
// REF - http://shibashake.com/wordpress-theme/expand-the-wordpress-quick-edit-menu

function custom_field_meta_save_func($post_id, $meta_key) {
	if (isset($_POST[$meta_key]) && (get_post($post_id)->post_type != 'revision')) {
		$meta_key_val = esc_attr($_POST[$meta_key]);
		if (!empty($meta_key_val))
      update_post_meta($post_id, $meta_key, $meta_key_val);
    else
			delete_post_meta($post_id, $meta_key);		
  }
}


//---------------------------------------------------------------------------
// *********** 6: Add fields to the quick inline edit form ******************
//---------------------------------------------------------------------------

// REF - (steps 6c & 6d no longer work in WP3.3) - http://shibashake.com/wordpress-theme/expand-the-wordpress-quick-edit-menu
// REF - http://www.ilovecolors.com.ar/saving-custom-fields-quick-bulk-edit-wordpress/
// the 2nd ref has a lot of problems as well. Check the original sources - http://xref.yoast.com/trunk/wp-admin/js/inline-edit-post.dev.js.source.html & wordpress/wp-admin/admin-ajax.php

// Step 6a:
// Create the html for the edit box
//
// Eample usage:
/*
  add_action('quick_edit_custom_box',  function ($column_name, $post_type) {
	//if post is a custom post type and only during the first execution of the action quick_edit_custom_box
// only add it once; see http://snipplr.com/view/61790/fix-duplicate-fields-with-quickeditcustombox-hook-in-wordpress/
  global $post;
	if ($post->post_type == $post_type && did_action('quick_edit_custom_box') === 1): 
  ?>
    <fieldset class="inline-edit-col-left">
	  <div class="inline-edit-col">
    <?php 
    create_custom_field_quick_select($column_name, $post_type, TAX_SLUG_book_ref, __('Book'), get_book_list(__('no book selected')) );
//    create_custom_field_quick_select($column_name, $post_type, TAX_SLUG_section_type, __('Content Type'), ?? );
//    create_custom_field_quick_select($column_name, $post_type, TAX_SLUG_progress, __('Progress'), ?? );
    ?>
	  </div>
    </fieldset>
  <?php
  endif;
  }, 10, 2 );
*/

function create_custom_field_quick_select($column_name, $post_type, $meta_key, $label, $item_list) {
	if ($column_name != $meta_key) return;
	?>
		<label class="title" for="<?php echo $meta_key ?>"><?php echo $label ?></span>
		<input type="hidden" name="<?php echo $meta_key ?>_noncename" id="<?php echo $meta_key ?>_noncename" value="" />
		<select name="<?php echo $meta_key ?>" id="<?php echo $meta_key ?>">
			<?php foreach ($item_list as $key => $title): ?>
        <option value="<?php echo $key ?>"><?php echo $title ?></option>
			<?php endforeach; ?>
		</select>
	<?php
}

function create_custom_field_quick_edit_box($column_name, $post_type, $meta_key, $label) {
	if ($column_name != $meta_key) return;
	?>
		<label class="title" for="<?php echo $meta_key ?>"><?php echo $label ?></span>
		<input type="hidden" name="<?php echo $meta_key ?>_noncename" id="<?php echo $meta_key ?>_noncename" value="" />
		<input type="text" name="<?php echo $meta_key ?>" id="<?php echo $meta_key ?>" size="10" value="">
	<?php
}






// Step 6b:
// is saved via the add_action( 'save_post', function ( $post_id ) above

// Step 6c:
// Update the initial value with javascript

// Example usage: (call for each  meta key to be edited)
/*
  add_action('admin_head-edit.php', function () {
    global $post;
    $post_type = POST_TYPE_book_contents;
    $meta_key = TAX_SLUG_book_ref;
	  if ($post->post_type != $post_type) return;
    create_javascript_for_select($post_type, $meta_key);
  } );
*/

function create_javascript_for_select($post_type, $meta_key) {
  ?>
  <script type="text/javascript">
    jQuery(document).ready(function($) {
      jQuery("a.editinline").on("click", function(event) {
        var quick_edit_post_id = inlineEditPost.getId(this); // get id for the post we are editing
        jQuery.post("<?php echo get_stylesheet_directory_uri() ?>/library/post_types_helper.php",
          { post_id: quick_edit_post_id, meta_key: "<?php echo $meta_key ?>", edit_action: "ajaxget" },
         	function(data) { 
// Can't get any query to work except option[value='some text']
//            var selObj1 = jQuery("select#<?php echo $meta_key ?> option:selected");
//            var selVal1 = selObj1.text();
//            var selAttr1 = selObj1.attr( 'selected' );
            if (data) {
              var selObj = jQuery("#<?php echo $meta_key ?> option[value="+data+"]");
              selObj.attr( 'selected', 'selected' );
            }
            else { // unselect the previously selected (from the last quick edit)
//              alert("Unselecting...");
//              var obj1 = jQuery("#<?php echo $meta_key ?> option:selected");
//              obj1.attr("selected", false);

// NOTE: since I can't get any other selector to work, there needs to be a zero option for this to unselect
              var obj2 = jQuery("#<?php echo $meta_key ?> option[value='0']");
              obj2.attr('selected', 'selected'); 
//              var obj3 = jQuery("#<?php echo $meta_key ?> option:first");
//              obj3.attr('selected', 'selected'); 
            }
          }
        );
      } );
    } );
  </script>
  <?php  
}



/*

Not working yet. Evidently, this is a hack to implement a second javascript-based button to save it. 
See http://www.ilovecolors.com.ar/saving-custom-fields-quick-bulk-edit-wordpress/


function create_javascript_for_bulk_select($post_type, $meta_key) {
  ?>
  <script type="text/javascript">
  var post_ids = [];
  var bulk_value = '';
  var post_ids_flat = '';

  jQuery(".bulk-updated").hide();

  jQuery('#doaction, #doaction2').click(function(e){
	  var n = jQuery(this).attr('id').substr(2);
	  if ( jQuery('select[name="'+n+'"]').val() == 'edit' ) {
		  e.preventDefault();
		  jQuery('tbody th.check-column input[type="checkbox"]:checked').each(function(i){
			  post_ids.push(jQuery(this).val());						
		  });
		
	  } else if ( jQuery('form#posts-filter tr.inline-editor').length > 0 ) {
		  t.revert();
	  }
  });

  jQuery(".bulk-update").on("click", function() {
	  bulk_value = jQuery(("#<?php echo $meta_key ?>")).val();
	  for (var i = 0; i < post_ids.length; i++) {
		  post_ids_flat = post_ids_flat + " " + post_ids[i];
	  }  
    jQuery.post("<?php echo get_stylesheet_directory_uri() ?>/library/post_types_helper.php",		  
      { post_ids : post_ids_flat, meta_key: "<?php echo $meta_key ?>", meta_val: bulk_value, edit_action: "ajaxsave" },
		  function(data){
			  jQuery(".bulk-updated").fadeIn(300).delay(800).fadeOut(300);
			  jQuery("input#bulk_edit").click();
		  }
	  );
  });
  </script>
  <?php
}

*/
