<?php

require_once('post_types_helper.php');

define ('POST_TYPE_book_contents', 'book_contents');

define ('TAX_SLUG_section_type', 'section_type');
define ('META_book_ref', 'book_ref');
define ('TAX_SLUG_progress', 'progress');


//---------------------------------------------------------------------------
// *********** Helper funcs *************************************************
//---------------------------------------------------------------------------

function get_book_list($empty_value = '') {
  $books = get_posts(array( 'post_type' => 'book', 'showposts' => -1 ));
  $meta_values = array();
  if (!empty($empty_value))
    $meta_values[] = $empty_value;
  foreach ($books as $book)
    $meta_values[$book->post_name] = $book->post_title;

  return $meta_values;
}

function get_post_word_count($post_id) {
  global $wpdb;

  $sql = "SELECT post_content FROM $wpdb->posts WHERE ID = '$post_id'";
	
	$posts = $wpdb->get_results($sql);
  $total_count = 0;
	if ($posts) {
		foreach ($posts as $post) {
			$content = strip_tags($post->post_content);
			$words = explode(' ', $content);
			$count = count($words);
      if ($count == 1) $count = 0; // explode for empty content will return 1
			$total_count .= $count;
		}
	}
	return number_format($total_count);
}

//---------------------------------------------------------------------------
// *********** 1: Create the Custom Post Type *******************************
//---------------------------------------------------------------------------

// REFS:
// http://thinkvitamin.com/code/create-your-first-wordpress-custom-post-type/

add_action( 'init', function () {
  register_post_type( POST_TYPE_book_contents,
    array(
      'labels' => array(
        'name' => __('Contents'),
        'singular_name' => __('Content'),
      ),
      'description' => __('Contains contents of books - sections, chapters...'),
      'public' => true,
      'menu_position' => 6,
      //'menu_icon' => get_stylesheet_directory_uri() . '/images/super-duper.png',
      'hierarchical' => true,
      'rewrite' => array( 'slug' => 'book-contents', 'with_front' => true ),
      //'taxonomies' => array( 'post_tag', 'category', 'section'),
      'supports' => array('title','editor', 'page-attributes', 'author', 'comments', 'revisions', 'post-formats')
    )
  );
} );



//---------------------------------------------------------------------------
// *********** 2: Create the Taxonomies *************************************
//---------------------------------------------------------------------------

add_action( 'init', function () {

  register_taxonomy(TAX_SLUG_section_type, 
    array(POST_TYPE_book_contents),  // apply to only this post_type
    array(
    'hierarchical' => false,
    'label' => __('Types'),
    'singular_label' => __('Type'),
    'show_ui' => true,
    'query_var' => true,
    'rewrite' => true, //array( 'slug' => 'section' ),
  ));

  register_taxonomy(TAX_SLUG_progress, 
    array(POST_TYPE_book_contents),  // apply to only this post_type
    array(
    'hierarchical' => false,
    'label' => __('Progress'),
    'singular_label' => __('Progress'),
    'show_ui' => true,
    'query_var' => true,
    'rewrite' => true, //array( 'slug' => 'section' ),
  ));

}, 0);


//---------------------------------------------------------------------------
// *********** 3: Create the NEW List Post Columns **************************
//---------------------------------------------------------------------------

add_filter( 'manage_edit-'.POST_TYPE_book_contents.'_columns', function ($defaults) {

  $newcols = array(); // create new columns array

//  $newcols['book_ref_old'] = __('Book_old');
  $newcols[META_book_ref] = __('Book');
  $newcols[TAX_SLUG_section_type] = __('Type');
  $newcols['progress'] = __('Progress');
  $newcols['words'] = __('Words');
  $newcols['order'] = __('Order');
  $newcols['password'] = __('Password');

  return insert_new_cols($defaults, 2, $newcols);

} );


// 3b: provide html & values to display each of our custom columns

// -- must use manage_page_custom_column for hierarchical post types, and manage_post_custom_column for non-hierarchical types
// - see http://codex.wordpress.org/Plugin_API/Action_Reference/manage_posts_custom_column
add_action('manage_pages_custom_column', function ($column, $post_id) {
  global $wpdb;
  global $gBookContentOptions;

  switch ($column) {
//      case "book_ref_old":
//        echo get_the_term_list($post_id, 'book_ref', '', ', ','');
//        break;
      case META_book_ref:
        $slug = get_post_meta($post_id, META_book_ref, true);
        if (!empty($slug)) {
          $posts = get_posts(array( 'name'=>$slug, 'post_type' => 'book', 'showposts' => 1 ));
          echo ($posts)? $posts[0]->post_title : '';
        } 
        else echo '';
        break;
      case "section_type":
        echo get_the_term_list($post_id, TAX_SLUG_section_type, '', ', ','');
        break;
      case "progress":
        echo get_the_term_list($post_id, 'progress', '', ', ','');
        break;
      case "words":
        echo get_post_word_count($post_id);
        break;
      case "order":
        echo get_post($post_id)->menu_order;
        break;
      case 'password':
        echo get_post($post_id)->post_password;
        break;
  }
} , 10, 2);

//---------------------------------------------------------------------------
// *********** 4: Create the NEW Post Listing Filters **********************
//---------------------------------------------------------------------------

// REF - from Charles Alexandre (near the bottom) - http://wordpress.stackexchange.com/questions/578/adding-a-taxonomy-filter-to-admin-list-for-a-custom-post-type

// Step 4a: define the filter combo-boxes (drop-down for the sections column for filtering posts)

// book-ref
/*add_action('restrict_manage_posts', function () {
  create_taxonomy_filter_dropdown(POST_TYPE_book_contents, META_book_ref, array('hide_empty' => false));
} );*/

add_action('restrict_manage_posts', function () {
  $meta_values = get_book_list();
  create_custom_field_dropdown(POST_TYPE_book_contents, META_book_ref, $meta_values, __("Show All Books") );
} );


// section-type
add_action('restrict_manage_posts', function () {
  create_taxonomy_filter_dropdown(POST_TYPE_book_contents, TAX_SLUG_section_type, array('hide_empty' => false));
} );

// progress
add_action('restrict_manage_posts', function () {
  create_taxonomy_filter_dropdown(POST_TYPE_book_contents, TAX_SLUG_progress, array('hide_empty' => false));
} );

// author
add_action('restrict_manage_posts', function () {
  create_author_dropdown(POST_TYPE_book_contents);
} );


// Step 4b: Define the actual filters funcs

add_filter('parse_query', function ($query) {
  custom_field_filter($query, POST_TYPE_book_contents, META_book_ref);
} );

// this one func does the filtering for all taxonomies
add_filter('parse_query', function ($query) {
  taxonomy_filter($query, POST_TYPE_book_contents);
} );



//---------------------------------------------------------------------------
// *********** 5: Create custom data types & widgets for edit post page *****
//---------------------------------------------------------------------------

// 5a: Create the widgets (meta-bax), with their respective html def funcs

add_action('add_meta_boxes', function () {
//  remove_meta_box('tagsdiv-'.META_book_ref, POST_TYPE_book_contents, 'side' );
  remove_meta_box('tagsdiv-'.TAX_SLUG_section_type, POST_TYPE_book_contents, 'side' );
  remove_meta_box('tagsdiv-'.TAX_SLUG_progress, POST_TYPE_book_contents, 'side' );
/*
  // meaning: add_meta_box( $id, $title, $callback, $post_type, $context, $priority )
  add_meta_box('tagsdiv-'.TAX_SLUG_book_ref.'_old', __('Book_old'), 
    function ($post) {  create_taxonomy_meta_box($post, TAX_SLUG_book_ref.'_old', __('Select which book the post belongs to')); }, 
    POST_TYPE_book_contents, 'side', 'default');
*/
  add_meta_box('tagsdiv-'.META_book_ref, __('Book'), function ($post) {
      $meta_values = get_book_list(__('no book selected'));
      create_custom_field_select_meta_box($post, META_book_ref, $meta_values, '', __('Select which book the post belongs to')
    ); }, POST_TYPE_book_contents, 'side', 'default');

  add_meta_box(
    'tagsdiv-'.TAX_SLUG_section_type, __('Section Type'), 
    function ($post) {  create_taxonomy_meta_box($post, TAX_SLUG_section_type, __('Select the content section type')); }, 
    POST_TYPE_book_contents, 'side', 'default');

  add_meta_box('tagsdiv-'.TAX_SLUG_progress, __('Progress'), 
    function ($post) {  create_taxonomy_meta_box($post, TAX_SLUG_progress, __('Select the stage the post is at')); }, 
    POST_TYPE_book_contents, 'side', 'default');
} );


// 5b: Define the widget's save func

add_action( 'save_post', function ( $post_id ) {
	if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) 
		return $post_id;	
	// Check permissions
	if ( !current_user_can( 'edit_page', $post_id ) )
		return $post_id;
	// OK, we're authenticated: we need to find and save the data
//  taxonomy_widget_save_func($post_id, TAX_SLUG_book_ref.'_old');
  custom_field_meta_save_func($post_id, META_book_ref);
  taxonomy_widget_save_func($post_id, TAX_SLUG_section_type);
  taxonomy_widget_save_func($post_id, TAX_SLUG_progress);
} );

// 5c: filter the page attribute widget list - limit to current book
// REF - http://codex.wordpress.org/Function_Reference/wp_dropdown_pages
add_filter( 'page_attributes_dropdown_pages_args', function ( $dropdown_args, $post ) {

  // first find out what book (slug) it belongs to:
  $book_slug = get_post_meta($post->ID, META_book_ref, true);

  $args = array(
    'post_type' => POST_TYPE_book_contents,
    TAX_SLUG_section_type => 'book',
    'meta_key' => META_book_ref,
    'meta_value' => $book_slug,
  );
  $books = get_posts( $args );
  // check for content start - section=book
  $numBooks = count($books);
  if (count($books) != 1) {
    $dropdown_args['child_of'] = 0;
    $dropdown_args['depth'] = 1; // show only the top level entries
  }
  else {
    $book = $books[0];
    $dropdown_args['child_of'] = $book->ID;
    //$dropdown_args['include'] = $book->ID;
    $dropdown_args['show_option_none'] = __('Book: ') . $book->post_title;
		$dropdown_args['option_none_value'] = $book->ID;
  }

  return $dropdown_args;
}, 10, 2 );


//---------------------------------------------------------------------------
// *********** 6: Add fields to the quick inline edit form ******************
//---------------------------------------------------------------------------

// REF - (steps 6c & 6d no longer work in WP3.3) - http://shibashake.com/wordpress-theme/expand-the-wordpress-quick-edit-menu
// REF - http://www.ilovecolors.com.ar/saving-custom-fields-quick-bulk-edit-wordpress/

// Step 6a:
// Create the html for the edit box

add_action('quick_edit_custom_box',  function ($column_name, $post_type) {
//if post is a custom post type and only during the first execution of the action quick_edit_custom_box
// only add it once; see http://snipplr.com/view/61790/fix-duplicate-fields-with-quickeditcustombox-hook-in-wordpress/
global $post;
if ($post->post_type == $post_type && did_action('quick_edit_custom_box') === 1): 
?>
  <fieldset class="inline-edit-col-left">
  <div class="inline-edit-col">
  <?php 
  create_custom_field_quick_select($column_name, $post_type, META_book_ref, __('Book'), get_book_list(__('no book selected')) );
//    create_custom_field_quick_select($column_name, $post_type, TAX_SLUG_section_type, __('Content Type'), ?? );
//    create_custom_field_quick_select($column_name, $post_type, TAX_SLUG_progress, __('Progress'), ?? );
  ?>
  </div>
  </fieldset>
<?php
endif;
}, 10, 2 );

/*
Not working yet. Evidently, it requires a second javascript-based button to save it. 
See http://www.ilovecolors.com.ar/saving-custom-fields-quick-bulk-edit-wordpress/
add_action('bulk_edit_custom_box', function ($column_name, $post_type) {
global $post;
if ($post->post_type == $post_type && did_action('bulk_edit_custom_box') === 1): 
?>
<fieldset class="inline-edit-col-left">
<div class="inline-edit-col">
<?php 
create_custom_field_quick_select($column_name, $post_type, META_book_ref, __('Book'), get_book_list(__('no book selected')) );
?>
</div>
<p class="bulk-updated"><?php echo __('Bulk data updated') ?></p>
<br/><a href="#" class="bulk-update button-secondary">Update Book Ref</a>
</fieldset>
<?php
endif;
}, 10, 2);
*/



// Step 6b:
// is saved via the add_action( 'save_post', function ( $post_id ) above

// Step 6c:
// Update the initial value with javascript

  add_action('admin_head-edit.php', function () {
    global $post;
    $post_type = POST_TYPE_book_contents;
    $meta_key = META_book_ref;
	  if ($post->post_type != $post_type) return;
    create_javascript_for_select($post_type, $meta_key);
    //create_javascript_for_bulk_select($post_type, $meta_key);
  } );





