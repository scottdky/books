<?php

require_once('post_types_helper.php');

define ('POST_TYPE_book', 'book');


//---------------------------------------------------------------------------
// *********** 1: Create the Custom Post Type *******************************
//---------------------------------------------------------------------------

// REFS:
// http://thinkvitamin.com/code/create-your-first-wordpress-custom-post-type/

add_action( 'init', function () {
  register_post_type( POST_TYPE_book,
    array(
      'labels' => array(
        'name' => __('Books'),
        'singular_name' => __('Book'),
      ),
      'description' => __('Information for each book in our catalog - slug, Title, Author(s), ISBN, ...'),
      'public' => true,
      'menu_position' => 5,
      //'menu_icon' => get_stylesheet_directory_uri() . '/images/super-duper.png',
      'hierarchical' => false,
      'rewrite' => array( 'slug' => 'book', 'with_front' => true ),
      //'taxonomies' => array( 'post_tag', 'category', 'section'),
      'supports' => array('title','editor', 'page-attributes', 'author', 'comments', 'revisions', 'post-formats', 'thumbnail')
    )
  );
} );


//---------------------------------------------------------------------------
// *********** 2: Create the Taxonomies *************************************
//---------------------------------------------------------------------------


//---------------------------------------------------------------------------
// *********** 3: Create the NEW List Post Columns **************************
//---------------------------------------------------------------------------

// 3a: Define the extra columns and where they go
add_filter( 'manage_edit-'.POST_TYPE_book.'_columns', function ($defaults) {

  $newcols = array(); // create new columns array

  $newcols['slug'] = __('Slug');

  return insert_new_cols($defaults, 2, $newcols);

} );

// 3b: provide html & values to display each of our custom columns
add_action('manage_posts_custom_column', function ($column, $post_id) {
  global $wpdb;

  switch ($column) {
      case "slug":
        echo get_post($post_id)->post_name;
        break;
  }
}, 10, 2);

//---------------------------------------------------------------------------
// *********** 4: Create the NEW Post Listing Filters **********************
//---------------------------------------------------------------------------



//---------------------------------------------------------------------------
// *********** 5: Create custom data types & widgets for edit post page *****
//---------------------------------------------------------------------------


// 5a: Create the widgets (meta-bax), with their respective html def funcs

// manadtory fields
// authors
// language
// identifier - isbn
// publisher
// publish date

// optional fields
// website
// subject
// contributers
// keywords
// source
// coverage
// rights



add_action("admin_init", function ()
{
  // meaning: add_meta_box( $id, $title, $callback, $post_type, $context, $priority )
  add_meta_box("help_info", __("Important Information"), "important_func", POST_TYPE_book, "normal", "default");
  add_meta_box("publish_info", __("Publishing Info (mandatory)"), "publish_info_func_man", POST_TYPE_book, "normal", "default");
  add_meta_box("optional_info", __("Publishing Info (optional)"), "publish_info_func_opt", POST_TYPE_book, "normal", "default");
  add_meta_box("stylesheet", __("Stylesheet (css)"), "stylesheet_func", POST_TYPE_book, "normal", "default");
} );
 
function important_func() {
?>
Please read carefully. You must provide a certain amount of minimum information in order for exporting to work properly. Failure to do so could result in files not comforming to Amazon's and other publishing standards. Please note:<br/><br/>

1) Your book cover should be a single .jpg image including any text you want to appear on it. This cover should be uploaded as the featured image.<br/>
2) The manadatory data will be automatically generated if you fail to provide any, but you need to make sure these defaults are what you trully want.<br/>
3) The optional fields are just that. You will probably want to fill in at least the website, subject, keywords (genre), and rights (copyright).<br/>
4) You will need to supply the css information for your book in the Stylesheet box. Older mobi (Kindle) and epub formats will only recognize simple css (no floats or positioning). The newer Amazon format (Kindle Format 8 - for Kindle Fire) supports much of html5 and CSS3 specifications. You need to understand which format you are wanting to support before creating your CSS. You can get more information on <a href="http://www.amazon.com/gp/feature.html/ref=amb_link_359603402_1?ie=UTF8&docId=1000729511&pf_rd_m=ATVPDKIKX0DER&pf_rd_s=right-4&pf_rd_r=0K6Q9RY16WFVHVJ2PW4D&pf_rd_t=1401&pf_rd_p=1342417002&pf_rd_i=1000765211">Kindle 8 format here.</a>
<?php
}

function publish_info_func_man(){
  global $post;
  create_custom_field_input_meta_box($post, 'authors', __('Authors:'), __('List of primary authors. Separate by commas, e.g. author1, author2 (default=WP User)'), 'custom-widget-long');
  create_custom_field_input_meta_box($post, 'language', __('Language:'), __('language code (default=en)'));
  create_custom_field_input_meta_box($post, 'identifier', __('Unique Identifier (usually ISBN#):'), __("If don't have a ISBN, choose some other unique code"));
  create_custom_field_input_meta_box($post, 'publisher', __('Publisher:'), __('default="Provide Your Own - Publishing"'));
  create_custom_field_input_meta_box($post, 'publish_date', __('Publish Date:'), __('The date you choose to publish. Use this form (stuff in [] is optional): YYYY[-MM[-DD]]'));
}

function publish_info_func_opt(){
  global $post;
  create_custom_field_input_meta_box($post, 'website', __('Website:'), __("Author's website (default=Books.ProvideYourOwn.com)"));
  create_custom_field_input_meta_box($post, 'subject', __('Subject:'), __('Phrase describing the subject'));
  create_custom_field_input_meta_box($post, 'contributers', __('Contributers:'), __("List of non-primary contributers. Separate by commas, e.g. contrib1, contrib2"), 'custom-widget-long');
  create_custom_field_input_meta_box($post, 'keywords', __('Keywords:'), __('List of keywords (genres). Separate by commas, e.g. key phrase1, key phrase2"'), 'custom-widget-long');
  create_custom_field_input_meta_box($post, 'source', __('Source:'), __('If the work is a derivative, cite original source here'));
  create_custom_field_input_meta_box($post, 'coverage', __('Coverage:'), __('Scope of publishing range'));
  create_custom_field_input_meta_box($post, 'rights', __('Rights:'), __('Copyright/Copyleft notice - one line only'));
}

function stylesheet_func() {
  global $post;
  create_custom_field_textarea_meta_box($post, 'stylesheet', __('Stylesheet:'), __("Stylesheet for your book - use normal css, but be aware of limitations for older Kindles and other e-readers"));
}
 
// 5b: Define the widget's save func

add_action('save_post', function () {
  global $post;
 
  $single_fields = array( 
    'language',
    'identifier',
    'publisher',
    'publish date',
    'website',
    'subject',
    'source',
    'coverage',
    'rights',
    'stylesheet',
  );

  $multiple_fields = array( 
    'authors',
    'contributers',
    'keywords',
  );

  // single fields
  foreach( $single_fields as $field ) {
    if (isset($_POST[$field]) && $_POST[$field] != "")
      update_post_meta($post->ID, $field, $_POST[$field]);
  }
  // multiple fields
  foreach( $multiple_fields as $field ) {
    if (isset($_POST[$field]) && $_POST[$field] != "") {
      $data = explode ( ',' , $_POST[$field] );
      foreach($data as $key => $val)
        $data[$key] = trim($val);
      update_post_meta($post->ID, $field, $data);
    }
  }
} );

