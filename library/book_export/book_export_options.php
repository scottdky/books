<?php  
  require_once('book_export_funcs.php');

  $type = 'kindle'; // set default

  // get post data first
  $book = $_POST['book'];
  $type = $_POST['type'];

// now draw the form
?>

<div class="wrap">
<h2>Book Export Tool</h2>

<form method="post" name="book_export_tool">

  <?php $books = getListOfBooks(); ?>
  <h3>Book To Export:</h3>
  <select name="book">
  <?php foreach( $books as $post ) : ?>
    <?php $slug = $post->post_name; ?>
    <option  
      <?php echo ($book !== '' && $slug==$book)? 'selected="selected"' : '' ?>
      value="<?php echo $slug ?>"><?php echo $post->post_title ?>
    </option>
  <?php endforeach; ?>
  </select>

  <h3>File Type:</h3>
  <input type="radio" name="type" value="mobi" <?php echo ('mobi'==$type)? 'checked':''?>> Kindle (Mobi)<br/>
  <input type="radio" name="type" value="epub" <?php echo ('epub'==$type)? 'checked':''?>> EPUB<br/>



	<input type="hidden" name="submitted" />
	<p class="submit">
	<input type="submit" name="export" class="button-primary" value="<?php echo __('Create') ?>" />
	</p>
</form> <br/>
<h3>Results:</h3>

<?php
// check for form submission and print results
if ($_POST['export']) {
    
  $error = exportBook($book, $type);
  if (!$error)
    echo "Success - Book has been exported.";
  else
    echo "Error occurred: $error";
}

print "</div>";
  

