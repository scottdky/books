<?php

class TableOfContents {

  public function __construct ( $identifier, $docTitle ) {
    $this->p_indentifier = $identifier; // isbn
    $this->p_doctitle = $docTitle;

    $this->p_index = 1; 
    $this->p_html = '';
    $this->p_ncx = '';
    $this->p_opf_manifest = '';
    $this->p_opf_spine = '';

  // the toc itself is first in the ncx file - create it
    $this->p_ncx .= $this->get_ncx_entry('TableOfContents', __('Table of Contents'));
  }

  public function createFiles($dir_name) {
    $htmlSize = $this->createHtmlFile($dir_name);
    $ncxSize = $this->createNCXFile($dir_name);
    return (($htmlSize == 0) || ($ncxSize == 0))? "error creating table of contents files" : '';
  }

  public function addEntry($slug, $title) {
    $this->p_html .= $this->get_html_entry($slug, $title);
    $this->p_ncx .= $this->get_ncx_entry($slug, $title);
    $this->p_opf_manifest .= $this->get_opf_entry_manifest($slug, $title);
    $this->p_opf_spine .= $this->get_opf_entry_spine($slug, $title);
  }

  public function getOPFManifest() {
    return $this->p_opf_manifest;
  }
  public function getOPFSpine() {
    return $this->p_opf_spine;
  }

  private function get_html_entry($slug, $title) {
    ob_start();
    ?>
<p class="p_toc" style = "margin-left: 20;"><a href="<?php echo $slug . '.html' ?>"><?php echo $title ?></a></p>
    <?php
    $output = ob_get_contents();
    ob_end_clean();
    return $output;
  }

  private function get_ncx_entry($slug, $title) {
    ob_start();
    ?>
        <navPoint id="navPoint-<?php echo $this->p_index ?>" playOrder="<?php echo $this->p_index ?>">
            <navLabel>
                <text><?php echo $title ?></text>
            </navLabel>
            <content src="<?php echo $slug . '.html' ?>" />
        </navPoint>
    <?php
    $output = ob_get_contents();
    ob_end_clean();
    $this->p_index++;
    return $output;
  }

  private function get_opf_entry_manifest($slug, $title) {
    ob_start();
    ?>
    <item id="<?php echo $title ?>" href="<?php echo $slug . '.html' ?>"
          media-type="application/xhtml+xml" />
    <?php
    $output = ob_get_contents();
    ob_end_clean();
    return $output;
  }

  private function get_opf_entry_spine($slug, $title) {
    ob_start();
    ?>
    <itemref idref="<?php echo $title ?>" />
    <?php
    $output = ob_get_contents();
    ob_end_clean();
    return $output;
  }

  //****************************** private funcs ****************************************

  private function createHtmlFile($dir_name) {
    $fileName = $dir_name . '/' . __('TableOfContents.html');
    $content = $this->getHtmlContent();
    $size = file_put_contents($fileName, $content);
    return $size;
  }

  private function createNCXFile($dir_name) {
    $fileName = $dir_name . '/' . __('toc.ncx');
    $content = $this->getNCXContent();
    $size = file_put_contents($fileName, $content);
    return $size;
  }

  private function getHtmlContent() {
    ob_start();
    ?>
<?php echo '<?xml version="1.0" encoding="utf-8" ?>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="application/xhtml+xml; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="style.css" />
<title><?php echo __("Table of Contents") ?></title>
</head>
<body>
<h3 class="toc_heading"><?php echo __("Table of Contents") ?></h3>
<?php echo $this->p_html ?>
</body>
</html>
    <?php
    $output = ob_get_contents();
    ob_end_clean();
    return $output;
  }

  private function getNCXContent() {
    ob_start();
    ?>
<?php echo '<?xml version="1.0" encoding="utf-8" ?>' ?>
<!DOCTYPE ncx PUBLIC "-//NISO//DTD ncx 2005-1//EN" "http://www.daisy.org/z3986/2005/ncx-2005-1.dtd">
<ncx xmlns="http://www.daisy.org/z3986/2005/ncx/" xml:lang="en" version="2005-1">
    <head>
        <meta name="dtb:uid" content="<?php echo $this->p_indentifier ?>" />
        <meta name="dtb:depth" content="1" />
        <meta name="dtb:totalPageCount" content="0" />
        <meta name="dtb:maxPageNumber" content="0" />
    </head>

    <docTitle>
        <text><?php echo $this->p_doctitle ?></text>
    </docTitle>

    <navMap>
<?php echo $this->p_ncx ?>    
    </navMap>
</ncx>
    <?php
    $output = ob_get_contents();
    ob_end_clean();
    return $output;
  }

  //******************************** data *******************************

  private $p_index;
  private $p_indentifier; // isbn
  private $p_doctitle;
  private $p_html;
  private $p_ncx;
  private $p_opf_manifest = '';
  private $p_opf_spine = '';
}
