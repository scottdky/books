<?php
  require_once('helper_funcs.php');
  require_once('TableOfContents.class.php');
  require_once('Book.class.php');

$book_export_debug = 'testing'; // set to true to show debug info

function getListOfBooks() {
  $args = array(
     'post_type' => 'book',
  );
  $books = get_posts( $args );
  return $books;
}

function getPostAsHtml($post) {
  ob_start();
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
  <head>
    <meta http-equiv="CONTENT-TYPE" content="text/html; charset=UTF-8">
    <title><?php echo $post->post_title; ?></title>
  </head>
  <body dir="LTR" lang="en-US">
    <h1><?php echo $post->post_title; ?></h1>
    <?php echo $post->post_content; ?>
  </body>
</html>

<?php
  $output = ob_get_contents();
  ob_end_clean();
  return $output;
}

function exportPostAsHtml($dir_name, $post) {
  $file_name = $dir_name . '/' . $post->post_name . '.html';
  $content = getPostAsHtml($post);
  $size = file_put_contents($file_name, $content);
  return $size;
}

function processBookSections($parent, $query_args, $dir_name, $toc) {
  $error = 0;
  $query_args['post_parent'] = $parent->ID;
  $contents = get_children( $query_args );
  foreach ($contents as $post) {
    // 1: write the post to a file
    echo "Processing section {$post->post_title} <br/>";
    $size = exportPostAsHtml($dir_name, $post);
    if ($size == 0)
      return "file {$post->post_title} could not be written";
    // 2: add to the table of contents
    $toc->addEntry($post->post_name, $post->post_title);
    
    // next, see if the post has any children of its own
    $error = processBookSections($post, $query_args, $dir_name, $toc);
  }
  return $error;
}


function exportBook($book_slug, $export_type) {
  global $book_export_debug;

  $book = new Book($book_slug);
  $error = $book->getBookFields();
  if ($error) return $error;

  $args = array(
    'numberposts' => -1,
    'post_type' => 'book_contents',
    'section_type' => 'book',
    //'book_ref' => $book_slug,
    'meta_key' => 'book_ref',
    'meta_value' => $book_slug,
  );
//  echo var_dump($args);
  $books = get_posts( $args );
  // check for content start - section=book
  $numBooks = count($books);


  if ($book_export_debug) {
    $args['section_type'] = 'book';
    $books = get_posts( $args );
    $numBooks = count($books);
    echo "NumBooks (book): $numBooks <br/>";

    $args['section_type'] = 'section';
    $books = get_posts( $args );
    $numBooks = count($books);
    echo "NumBooks (section): $numBooks <br/>";

    $args['section_type'] = 'chapter';
    $books = get_posts( $args );
    $numBooks = count($books);
    echo "NumBooks (chapter): $numBooks <br/>";

    foreach ($books as $post)
      echo "Post: {$post->post_title} <br/>";

    $args['section_type'] = 'book';

    return;
  }
  else
    echo "Debug state: $book_export_debug <br/>";

  echo "Is debug defined: " . isset($book_export_debug) . '<br/>';



  if (!$numBooks)
    return "no book post belonging book slug $book_slug was found";
  else if ($numBooks > 1)
    return "more than one book ($numBooks) was found belonging to book slug $book_slug";

  reset($books);
  $current_section = current($books); // get the book post

  $args = array(
    'numberposts' => -1,
    'order' => 'ASC',
    'orderby' => 'menu_order',
    'post_type' => 'book_contents',
    'book_ref_category' => $book_slug,
  );

  echo "Processing book contents...<br/>";

  $dir_name = createBuildDirectory($book_slug);
  $toc = new TableOfContents($book->identifier, $book->title);
  $error = processBookSections($current_section, $args, $dir_name, $toc);
  if ($error) return $error;
  $error = $toc->createFiles($dir_name);
  if ($error) return $error;
  $error = $book->createCoverPageFile($dir_name);
  if ($error) return $error;
  $error = $book->createCSSFile($dir_name);
  if ($error) return $error;
  $error = $book->copyCoverImage($dir_name);
  if ($error) return $error;
  $error = $book->createOPFFile($dir_name, $toc);
  if ($error) return $error;
  echo "Generating E-book...<br/>";
  $error = $book->createEbook($dir_name, $export_type);
  if ($error > 1) return $error;
  echo '<br/>';
  echo "Success - Book has been exported.";

  $upload_info = wp_upload_dir();
  $url = $upload_info['baseurl'];
  $ebook = $url . '/book_exports/' . $book_slug . '/' . $book->ebook_file;
  echo sprintf('<script type="text/javascript">window.top.location="%s";</script>', $ebook);

  //$book->sendFileToUser($dir_name, $export_type);


  return $error;
}


