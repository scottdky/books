<?php

class Book {

  // manadtory fields
  public $slug;
  public $title;
  public $authors;
  public $lang;
  public $identifier;
  public $publisher;
  public $date;
  // optional fields
  public $website;
  public $subject;
  public $contributers;
  public $keywords;
  public $source;
  public $coverage;
  public $rights;

  public $css;

  public $cover_image;

  public $ebook_file; // the filename only - this is generated in the last step

  public function __construct ( $book_slug ) {
    $this->slug = $book_slug;
    $this->ebook_file = '';
  }

  private function getMetaField($post, $tag, $default) {
    $value = get_post_meta($post->ID, $tag, true);
    return (empty($value))? $default : $value;
  }
  /**
   * this func must be called to populate its data
   */
  public function getBookFields() {
    $args = array(
      'post_type' => 'book',
      'name' => $this->slug,
    );
    //echo var_dump($args);
    $books = get_posts( $args );
//    foreach ($books as $post)  {
//      echo '<br/><br/>';
//      echo "slug= '{$post->post_name}'<br/>";
//      echo "slug matches: " . ($post->post_name == $this->slug) . '<br/>';
//      var_dump($post);
//    }
    // check for content start - section=book
    $numBooks = count($books);
//echo "num books: $numBooks<br/>";
    if ($numBooks != 1)
      return __("No record of post type 'book' having slug= '{$this->slug}' was found");

    reset($books);
    $post = current($books); // get the book post

    echo __("Processing book: {$post->post_title}<br/>");
    $user = get_userdata($post->post_author);

    $this->title = $post->post_title;
    $this->authors = $this->getMetaField($post, 'authors', $user->user_firstname . ' ' .$user->user_lastname);
    $this->identifier = $this->getMetaField($post, 'identifier', $post->post_date_gmt);
    $this->lang = $this->getMetaField($post, 'language', 'en');
    $this->publisher = $this->getMetaField($post, 'publisher', 'Provide Your Own - Publishing');
    $this->date = $this->getMetaField($post, 'publish_date', $post->post_date);
    // optional fields
    $this->website = $this->getMetaField($post, 'website', 'Books.ProvideYourOwn.com');
    $this->subject = $this->getMetaField($post, 'subject', '');
    $this->contributers = $this->getMetaField($post, 'contributers', '');
    $this->keywords = $this->getMetaField($post, 'keywords', '');
    $this->source = $this->getMetaField($post, 'source', '');
    $this->coverage = $this->getMetaField($post, 'coverage', '');
    $this->rights = $this->getMetaField($post, 'rights', '');

    $this->css = $this->getMetaField($post, 'stylesheet', '');

    $thumb_id = get_post_thumbnail_id( $post->ID ); 
    $this->cover_image = get_attached_file($thumb_id);

    if (!file_exists($this->cover_image))
      return __("cover image file does not exist - please specify a featured image for your book!");

    return 0;
  }

  public function createOPFFile($dir_name, $toc) {
    $filename = $dir_name . '/' . 'content.opf';
    $content = $this->getOPFContent($toc);
    $size = file_put_contents($filename, $content);
    return ($size == 0)? "error creating the .opf file" : '';
  }

  public function createCoverPageFile($dir_name) {
    $filename = $dir_name . '/' . 'CoverPage.html';
    $content = $this->getCoverPageContent();
    $size = file_put_contents($filename, $content);
    return ($size == 0)? "error creating the .opf file" : '';
  }

  public function createCSSFile($dir_name) {
    $filename = $dir_name . '/' . 'style.css';
    $size = file_put_contents($filename, $this->css);
    $src_size = strlen($this->css);
    // stylesheet may be empty, but it needs to at least exist
    if ($src_size > 0)
      $result = $size;
    else
      $result = file_exists($filename);
    return ($result == 0)? "error creating the .css file" : '';
  }

  public function copyCoverImage($dir_name) {
    $filename = $dir_name . '/' . 'CoverDesign.jpg';
    $success = copy( $this->cover_image, $filename );
    if (!$success)
      return __("error copying cover image file");
    else
      return 0;
  }

  public function createEbook($dir_name, $export_type) {
    $this->ebook_file = $this->slug . '.' . $export_type;
    if ($export_type == 'epub') {
      return $this->createEPubBook($dir_name);
    }
    else if ($export_type == 'mobi') { // kindle
      return $this->createKindleBook($dir_name);
    }
    else
      return 'unrecognized e-book type';
  }

  public function sendFileToUser($dir_name, $export_type) {
  // we can't use this function one the headers have been sent!!

    // code from example #1 - http://php.net/manual/en/function.header.php
    // We'll be outputting a PDF
    header("Content-type: application/$export_type");

    // We can use whatever name.ext that we want - it doesn't have to be the original one
    header(sprintf('Content-Disposition: attachment; filename="%s"', $this->ebook_file));

    // The PDF source is in original.pdf
    readfile($dir_name . '/' . $this->ebook_file);
  }

  private function createEPubBook($dir_name) {
    $output = '';
    $result = 0;
    //$command = "zip [-options] -b $dir_name [-t mmddyyyy] [-n suffixes] [zipfile list] [-xi list]";
    exec ( $command, $output, $result );
    return $result;
  }

  private function createKindleBook($dir_name) {
// to test via ssh:
// /var/www/books.provideyourown/html/wordpress/wp-content/themes/pyobooks-whiteboard/library/book_export/kindlegen/kindlegen content.opf -c1 -verbose -rebuild ebook.mobi

    $output = '';
    $result = 0;
    $opf_file = $dir_name . '/' . 'content.opf';
    $command = __DIR__ . "/kindlegen/kindlegen $opf_file -c1 -verbose -rebuild -o {$this->ebook_file}";
    exec ( $command, $output, $result );
  
    //if ($result)
    foreach($output as $str) echo "$str <br/>";
//      var_dump($output);
    return $result;
  }

  private function printODFMetaLine($tag, $field) {
      ?>
      <dc:<?php echo $tag ?>><?php echo $field ?></dc:<?php echo $tag ?>>
      <?php
  }

  private function printODFMeta($tag, $field) {
//echo "test: field= $field <br/>";
    if (!empty($field))
    {
      if (!is_array($field)) {
        $this->printODFMetaLine($tag, $field);
      } 
      else {
        foreach ($field as $item)
          $this->printODFMetaLine($tag, $item);          
      }
    }
  }

  private function getOPFContent($toc) {
    ob_start();
    ?>
<?php echo '<?xml version="1.0" encoding="UTF-8"?>' ?>
<package xmlns="http://www.idpf.org/2007/opf" unique-identifier="BookId" version="2.0">
    <metadata xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:dcterms="http://purl.org/dc/terms/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:opf="http://www.idpf.org/2007/opf">
    <?php
    // manadtory fields
    $this->printODFMeta('title', $this->title);
    $this->printODFMeta('creator', $this->authors);
    $this->printODFMeta('language', $this->lang);
    $this->printODFMeta('identifier', $this->identifier);
    $this->printODFMeta('publisher', $this->publisher);
    $this->printODFMeta('date', $this->date);
    // optional fields
    $this->printODFMeta('relation', $this->website);
    $this->printODFMeta('subject', $this->subject);
    $this->printODFMeta('contributer', $this->contributers);
    $this->printODFMeta('type', $this->keywords);
    $this->printODFMeta('source', $this->source);
    $this->printODFMeta('coverage', $this->coverage);
    $this->printODFMeta('rights', $this->rights);
    ?>
        <meta name="cover" content="CoverDesign"/>
    </metadata>
    <manifest>
        <item id="toc" href="toc.ncx"
          media-type="application/x-dtbncx+xml" />
        <item id="CoverDesign" href="CoverDesign.jpg"
          media-type="image/jpeg" />
        <item id="CoverPage" href="CoverPage.html"
          media-type="application/xhtml+xml" />
        <item id="TableOfContents" href="TableOfContents.html"
          media-type="application/xhtml+xml" />
        <item id="style" href="style.css"
          media-type="text/css" />
    <?php echo $toc->getOPFManifest(); ?>
    </manifest>
    <spine toc="toc">
        <itemref idref="CoverPage" linear="no" />
        <itemref idref="TableOfContents" />
    <?php echo $toc->getOPFSpine(); ?>
    </spine>
    <guide>
        <reference type="cover" title="<?php echo __('Cover Page') ?>"
            href="CoverPage.html" />
        <reference type="toc" title="echo <?php echo __('Table of Contents') ?>"
            href="TableOfContents.html" />
    </guide>
</package>
    <?php
    $output = ob_get_contents();
    ob_end_clean();
    return $output;    
  }

  private function getCoverPageContent() {
    ob_start();
    ?>
<?php echo '<?xml version="1.0" encoding="utf-8" ?>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="application/xhtml+xml; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="style.css" />
<title><?php echo __('Cover Page') ?></title>
</head>
<body style="margin-top: 10; padding: 0; oeb-column-number: 1;">
<p style="margin: 0; padding: 0; text-align: center">
<a href="<?php echo $this->website ?>"><img src="CoverDesign.jpg" height="96%" alt="Cover" /></a>
</p>
</body>
</html>
    <?php
    $output = ob_get_contents();
    ob_end_clean();
    return $output;    
  }

}
