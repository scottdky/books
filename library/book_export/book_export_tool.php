<?php
/*
Plugin Name: Book Export Tool
Plugin URI: 
Description: To be used in conjunction with pyo-books theme. Generates various book formats for a given book.
Author: Scott Daniels
Version: 1.0
Author URI: http://provideyourown.com
*/

/**
 * Add menu to admin/tools section
 * @return
 */
add_action('admin_menu', function () {
  add_management_page('Book Export Tool', 'Book Export', 'manage_options', 'book_export', 'book_export_options');
} );

/**
 * Include separate page for the options form
 * @return
 */
function book_export_options() {
	include 'book_export_options.php';
}



?>
