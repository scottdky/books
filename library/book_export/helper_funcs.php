<?php

/**
 * recursively emptiens and removes directories & files, include the start $dir
*/
function rrmdir($dir) {
 if (is_dir($dir)) {
   $objects = scandir($dir);
   foreach ($objects as $object) {
     if ($object != "." && $object != "..") {
       if (filetype($dir."/".$object) == "dir") rrmdir($dir."/".$object); else unlink($dir."/".$object);
     }
   }
   reset($objects);
   rmdir($dir);
 }
}

function createBuildDirectory($project_name) {
  // get WP's upload dir
  $upload_info = wp_upload_dir();
  $upload_dir = $upload_info['basedir'];
  
  // create our own subdir (if it doesn't exist)
  $upload_dir .= __('/book_exports');
  if (!is_dir($upload_dir))
    mkdir($upload_dir);

  // create the project subdir
  $dir_name = $upload_dir . '/' . $project_name;
  rrmdir($dir_name); // remove an old one if it exists
  mkdir($dir_name); // create the directory
  return $dir_name;
}

