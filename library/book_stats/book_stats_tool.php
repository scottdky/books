<?php
/*
Plugin Name: Book Stats Tool
Plugin URI: 
Description: To be used in conjunction with pyo-books theme. Provides various stats for a given book.
Author: Scott Daniels
Version: 1.0
Author URI: http://provideyourown.com
*/

/**
 * Add menu to admin/tools section
 * @return
 */
add_action('admin_menu', function () {
  add_management_page('Book Stats Tool', 'Book Stats', 'manage_options', 'book_stats', 'book_stats_options');
} );

/**
 * Include separate page for the options form
 * @return
 */
function book_stats_options() {
	include 'book_stats_options.php';
}



?>
