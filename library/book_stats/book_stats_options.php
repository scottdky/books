<?php  
//  require_once('book_stats_funcs.php');


// get post data first
$book = $_POST['book'];
$words_per_page = isset($_POST['words_per_page'])? $_POST['words_per_page'] : 400;


function getListOfBooks() {
  $args = array(
     'post_type' => 'book',
  );
  $books = get_posts( $args );
  return $books;
}

// now draw the form
?>


<div class="wrap">
<h2>Book Stats</h2>

<form method="post" name="book_stats_tool">

  <?php $books = getListOfBooks(); ?>
  <label for="book">Book To Show Stats:</label>
  <select name="book">
  <?php foreach( $books as $post ) : ?>
    <?php $slug = $post->post_name; ?>
    <option  
      <?php echo ($book !== '' && $slug==$book)? 'selected="selected"' : '' ?>
      value="<?php echo $slug ?>"><?php echo $post->post_title ?>
    </option>
  <?php endforeach; ?>
  </select>

  <br/><br/>
  <label for="words_per_page">Est. Words per Page: </label>
  <input type="text" name="words_per_page" value="<?php echo $words_per_page ?>" />

	<input type="hidden" name="submitted" />
	<p class="submit">
	<input type="submit" name="show" class="button-primary" value="<?php echo __('Show') ?>" />
	</p>
</form> <br/>
<h2>Results:</h2>

<?php
// check for form submission and print results
if ($_POST['show']) {
    
  global $wpdb;

  // get section & chapter counts - we'll just get all the posts - not as fast as an SQL count, but it doesn't matter here
//  $sql = "SELECT COUNT( * ) AS num_posts FROM {$wpdb->posts} WHERE post_type = 'book_contents' AND book_ref = '$book' AND section_type = 'chapter'";
//   $chapter_count = $wpdb->get_results($sql);

  $args = array(
    'numberposts' => -1,
    'post_type' => 'book_contents',
    // 'book_ref' => $book,
    'meta_key' => 'book_ref',
    'meta_value' => $book,
  );
  $args['section_type'] = 'section';
  $posts = get_posts( $args );
  $section_count = count($posts);

  $args['section_type'] = 'chapter';
  $posts = get_posts( $args );
  $chapter_count = count($posts);

  $total_char_count = 0;
  $total_word_count = 0;
  $chapter_word_count = array();
	if ($posts) {
		foreach ($posts as $chapter) {
			$content = strip_tags($chapter->post_content);
      $chars = strlen($content);
			$word_content = explode(' ', $content);
      $words = count($word_content);
			$total_word_count += $words;
      $total_char_count += $chars;
      $chapter_word_count[$chapter->post_title] = $words;
		}
	}
	$word_count = number_format($total_word_count);	
  $char_count = number_format($total_char_count);

  $page_count = ($section_count * 2) + $chapter_count + ($total_word_count / $words_per_page);
  $location_count = $total_char_count / 128; // http://booksprung.com/how-long-is-a-location-in-a-kindle-ebook

?>
  <h3>Word Counts by Chapter</h3>
  <?php foreach ($chapter_word_count as $title => $count): ?>
  <strong>Chapter - </strong><?php echo "$title - $count words <br/>" ?>
  <?php endforeach; ?>
  <br/>

  <strong>Number of Sections: </strong><?php echo $section_count ?><br/><br/>
  <strong>Number of Chapters: </strong><?php echo $chapter_count ?><br/><br/>

  <strong>Total Word Count: </strong><?php echo $word_count ?><br/><br/>
  <strong>Total Char Count: </strong><?php echo $char_count ?><br/><br/>

  <strong>Estimated Book Pages: </strong><?php echo number_format($page_count) ?><br/><br/>

  <strong>Estimated Kindle Locations: </strong><?php echo number_format($location_count) ?><br/><br/>

<?php
}

print "</div>";
  

