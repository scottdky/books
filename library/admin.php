<?php

//----------------------------------------------------------------------------
//*********************** Post Listing Pages *********************************
//----------------------------------------------------------------------------
// REFS:
// http://nspeaks.com/1015/add-custom-taxonomy-columns-to-edit-posts-page/
// http://thinkvitamin.com/code/create-your-first-wordpress-custom-post-type/



//****************** Add columns for Book  Listing *******************


// Define the extra columns and where they go
add_filter( 'manage_edit-book_columns', 'book_columns' );
function book_columns($defaults) {
  $cols = array(); // create new columns array

  // loop thru all columns, copying them, and inserting our in the order we want
  $index = 0;
  foreach($defaults as $key=>$val) {
    if ($index == 2) {
      $cols['slug'] = __('Slug');
    }
    $cols[$key] = $val;
    $index++;
  }
  return $cols;
}

// provide html & values to display each of our custom columns
// -- must use manage_page_custom_column (instead of *_post_** for hierarchical post types - see http://codex.wordpress.org/Plugin_API/Action_Reference/manage_posts_custom_column
add_action('manage_posts_custom_column', 'book_custom_column', 10, 2);
function book_custom_column($column, $post_id)
{
  global $wpdb;

  switch ($column) {
      case "slug":
        echo get_post($post_id)->post_name;
        break;
  }
}





//****************** Add columns for Book Content Listing *******************


$gBookContentOptions = array(__("First Draft"), __("Second Draft"), __("Review"), __("Third Draft"), __("Final Review"), __("Published"));

// Define the extra columns and where they go
add_filter( 'manage_edit-book_contents_columns', 'book_section_columns' );
function book_section_columns($defaults) {
  $cols = array(); // create new columns array

  // loop thru all columns, copying them, and inserting our in the order we want
  $index = 0;
  foreach($defaults as $key=>$val) {
    if ($index == 2) {
      $cols['book_ref'] = __('Book');
      $cols['section_type'] = __('Type');
      $cols['progress'] = __('Progress');
      $cols['order'] = __('Order');
      $cols['password'] = __('Password');
    }
    $cols[$key] = $val;
    $index++;
  }
  return $cols;
}

// provide html & values to display each of our custom columns
// -- must use manage_page_custom_column (instead of *_post_** for hierarchical post types - see http://codex.wordpress.org/Plugin_API/Action_Reference/manage_posts_custom_column
add_action('manage_pages_custom_column', 'book_section_custom_column', 10, 2);
function book_section_custom_column($column, $post_id)
{
  global $wpdb;
  global $gBookContentOptions;

  switch ($column) {
      case "book_ref":
        echo get_the_term_list($post_id, 'book_ref', '', ', ','');
        break;
      case "section_type":
        echo get_the_term_list($post_id, 'section_type', '', ', ','');
        break;
      case "progress":
        $progStr = get_post_meta($post_id, 'progress', true);
        echo ($progStr === '')? 'Unstarted' : $gBookContentOptions[$progStr];
        break;
      case "order":
        echo get_post($post_id)->menu_order;
        break;
      case 'password':
        echo get_post($post_id)->post_password;
        break;
//      case "section_slug":
//        echo $custom["section_slug"][0];
//        break;
  }
}


//**************** Add column filters for Book Content Listing *******************


// REF - from Charles Alexandre (near the bottom) - http://wordpress.stackexchange.com/questions/578/adding-a-taxonomy-filter-to-admin-list-for-a-custom-post-type


// this func adds a drop-down for the sections column for filtering posts
add_action('restrict_manage_posts', 'restrict_to_book_ref');
function restrict_to_book_ref() {
  global $typenow;
  $post_type = 'book_contents'; // change HERE
  $taxonomy = 'book_ref'; // change HERE
  if ($typenow == $post_type) {
    $selected = isset($_GET[$taxonomy]) ? $_GET[$taxonomy] : '';
    $info_taxonomy = get_taxonomy($taxonomy);
    wp_dropdown_categories(array(
      'show_option_all' => __("Show All {$info_taxonomy->label}"),
      'taxonomy' => $taxonomy,
      'name' => $taxonomy,
      'orderby' => 'name',
      'selected' => $selected,
      'show_count' => true,
      'hide_empty' => true,
    ));
  }
}

add_action('restrict_manage_posts', 'restrict_to_content_type');
function restrict_to_content_type() {
  global $typenow;
  $post_type = 'book_contents'; // change HERE
  $taxonomy = 'section_type'; // change HERE
  if ($typenow == $post_type) {
    $selected = isset($_GET[$taxonomy]) ? $_GET[$taxonomy] : '';
    $info_taxonomy = get_taxonomy($taxonomy);
    wp_dropdown_categories(array(
      'show_option_all' => __("Show All {$info_taxonomy->label}"),
      'taxonomy' => $taxonomy,
      'name' => $taxonomy,
      'orderby' => 'name',
      'selected' => $selected,
      'show_count' => true,
      'hide_empty' => true,
    ));
  }
}

// REF: http://wordpress.stackexchange.com/questions/16637/how-to-filter-post-listing-in-wp-dashboard-posts-listing-using-a-custom-field/16641#16641
add_action('restrict_manage_posts', 'restrict_to_progress');
function restrict_to_progress()
{
  global $wpdb;
  global $typenow;
  global $gBookContentOptions;

  $post_type = 'book_contents'; // change HERE
  $meta_key = 'progress'; // change HERE
  if ($typenow == $post_type) {
    $selected = isset($_GET[$meta_key]) ? $_GET[$meta_key] : '';
    $options = $gBookContentOptions;

    // get a count for each time an option is used
    $counts = array();
    $table_name = $wpdb->postmeta;
    foreach ($options as $key => $option) {
      $sql = "SELECT COUNT(*) FROM $wpdb->postmeta WHERE meta_key = %s AND meta_value = %s";
      // statement is from http://codex.wordpress.org/Class_Reference/wpdb
      $counts[$key] = $wpdb->get_var( $wpdb->prepare( $sql, $meta_key, $key ) );
    }

    ?>
    <select name="<?php echo $meta_key ?>">
    <option value=""><?php echo __('Show All Levels'); ?></option>
    <?php foreach ($options as $key => $option): ?>
      <option <?php echo ($selected !== '' && $key==$selected)? 'selected="selected"' : '' ?> value="<?php echo $key ?>"><?php echo $option . '(' . $counts[$key] . ')' ?></option>
    <?php endforeach; ?>
    </select> 
    <?php

  }
}


//****************************** Column Filter Funcs *************************************

//*********** Filter Taxonomy Columns; works for all taxonomies *********************

// REF - from Drew Gourley - http://wordpress.stackexchange.com/questions/578/adding-a-taxonomy-filter-to-admin-list-for-a-custom-post-type

// this function adds a wp filter for changing the results from id to slug for all taxonomies
add_filter('parse_query','taxonomy_convert_restrict');
function taxonomy_convert_restrict($query) {
    global $pagenow;
    global $typenow;
    if ($pagenow=='edit.php') {
        $filters = get_object_taxonomies($typenow);
        foreach ($filters as $tax_slug) {
            $var = &$query->query_vars[$tax_slug];
            if ( isset($var) ) {
                $term = get_term_by('id',$var,$tax_slug);
                $var = $term->slug;
            }
        }
    }
}

//*********** Filter Taxonomy Columns; works for all taxonomies *********************

// REF: http://wordpress.stackexchange.com/questions/16637/how-to-filter-post-listing-in-wp-dashboard-posts-listing-using-a-custom-field/16641#16641

add_filter('parse_query', 'progress_filter');
function progress_filter( $query )
{
  global $pagenow;
  $meta_key = 'progress'; // change HERE
  if ( is_admin() && ($pagenow=='edit.php') && (isset($_GET[$meta_key]) && $_GET[$meta_key] != '')) {
      $query->query_vars['meta_key'] = $meta_key;
      $query->query_vars['meta_value'] = $_GET[$meta_key];
  }
}




//----------------------------------------------------------------------------
//*********************** Post Edit Page *************************************
//----------------------------------------------------------------------------

// REFS:
// http://thinkvitamin.com/code/create-your-first-wordpress-custom-post-type/


//****************** Add custom data fields to our post types ********************


//****************** Book Post Type ****************************
add_action("admin_init", "book_custom_types");
function book_custom_types()
{
  // meaning: add_meta_box( $id, $title, $callback, $page, $context, $priority )
  add_meta_box("subtitle", "Subtitle", "subtitle_func", "book", "normal", "high");
  add_meta_box("publish_info", "Publishing Info", "publish_info_func", "book", "normal", "default");
}
 
function subtitle_func(){
  global $post;
  $custom = get_post_custom($post->ID);
  $value = $custom["subtitle"][0];
  ?>
  <label><strong>Subtitle:</strong></label>
  <input name="subtitle" value="<?php echo $value; ?>" />
  <?php
}

function publish_info_func(){
  global $post;
  $custom = get_post_custom($post->ID);
  $authors = $custom["authors"][0];
  $isbn = $custom["isbn"][0];
  ?>
  <label><strong>Authors:</strong></label>
  <input name="authors" value="<?php echo $authors; ?>" />
  <label><strong>ISBN:</strong></label>
  <input name="isbn" value="<?php echo $isbn; ?>" />
  <?php
}
 
add_action('save_post', 'save_book_metas');
function save_book_metas(){
  global $post;
 
  update_post_meta($post->ID, "subtitle", $_POST["subtitle"]);
  update_post_meta($post->ID, "authors", $_POST["authors"]);
  update_post_meta($post->ID, "isbn", $_POST["isbn"]);
}



//****************** Book Contents Post Type ****************************
add_action("admin_init", "book_contents_custom_types");
function book_contents_custom_types()
{
  // meaning: add_meta_box( $id, $title, $callback, $page, $context, $priority )
  add_meta_box("progress", "Progress", "progress_func", "book_contents", "side", "default");
}
 
function progress_func(){
  global $post;
  global $gBookContentOptions;

  $custom = get_post_custom($post->ID);
  $value = $custom["progress"][0];
  $options = $gBookContentOptions;
  ?>
  <label><strong>Progress:</strong></label>
  <select name="progress">
  <?php foreach ($options as $key => $option): ?>
    <option <?php echo ($key==$value)? 'selected="selected"' : '' ?> value="<?php echo $key ?>"><?php echo $option ?></option>
  <?php endforeach; ?>
  </select> 
  <?php
}

 
add_action('save_post', 'save_book_content_metas');
function save_book_content_metas(){
  global $post;
 
  update_post_meta($post->ID, "progress", $_POST["progress"]);
}


/*

// *************** A debug widget! **

add_action("admin_init", "debug_box");
function debug_box()
{
  // meaning: add_meta_box( $id, $title, $callback, $page, $context, $priority )
  add_meta_box("debug", "Debug Box", "debug_func", "book_contents", "side", "default");
}
 
function debug_func(){
  global $post;

	$post_type_object = get_post_type_object($post->post_type);
	if ( $post_type_object->hierarchical )
    echo "is hierachical";
  else  
    echo "fail to be hierarchical";
}



*/


