<?php

// NOTE: This is the section taxonomy support from PYO - it is unchanged (probably should be a plugin)




//********************* ADMIN extensions ***************************************

/*

//**   Add column in manage posts screen for Group taxononmy
//**   We should make this a plugin
// see http://nspeaks.com/1015/add-custom-taxonomy-columns-to-edit-posts-page/


add_filter( 'manage_posts_columns', 'section_columns' ); //Filter out Post Columns with 2 custom columns

function section_columns($defaults) {
  $cols = array();
  $index = 0;
  foreach($defaults as $key=>$val) {
    if ($index == 3) {
      $cols['section'] = __('Section');
    }
    $cols[$key] = $val;
    $index++;
  }
  return $cols;
}

add_action('manage_posts_custom_column', 'section_custom_column', 10, 2); //Just need a single function to add multiple columns

function section_custom_column($column_name, $post_id)
{
  global $wpdb;
  if( $column_name == 'section' ) {
    $tags = get_the_terms($post_id, 'section'); //lang is the first custom taxonomy slug
    if ( !empty( $tags ) ) {
      $out = array();
      foreach ( $tags as $c )
              $out[] = "<a href='edit.php?lang=$c->slug'> " . esc_html(sanitize_term_field('name', $c->name, $c->term_id, 'lang', 'display')) . "</a>";
      echo join( ', ', $out );
    } else {
      _e('No Section');  //No Taxonomy term defined
    }
  }
}


// ****************** now add admin filtering on posts page for section taxonomy ****************

// REF - from Charles Alexandre (near the bottom) - http://wordpress.stackexchange.com/questions/578/adding-a-taxonomy-filter-to-admin-list-for-a-custom-post-type
// this func adds a drop-down for the sections column for filtering posts
add_action('restrict_manage_posts', 'restrict_sections');
function restrict_sections() {
  //global $typenow;
  //$post_type = 'books'; // change HERE
  $taxonomy = 'section'; // change HERE
  //if ($typenow == $post_type) {
  $selected = isset($_GET[$taxonomy]) ? $_GET[$taxonomy] : '';
  $info_taxonomy = get_taxonomy($taxonomy);
  wp_dropdown_categories(array(
      'show_option_all' => __("Show All {$info_taxonomy->label}"),
      'taxonomy' => $taxonomy,
      'name' => $taxonomy,
      'orderby' => 'name',
      'selected' => $selected,
      'show_count' => true,
      'hide_empty' => true,
  ));
//  }
}

// REF - from Drew Gourley - http://wordpress.stackexchange.com/questions/578/adding-a-taxonomy-filter-to-admin-list-for-a-custom-post-type
// this function adds a wp filter for changing the results from id to slug for all taxonomies
add_filter('parse_query','taxonomy_convert_restrict');
function taxonomy_convert_restrict($query) {
    global $pagenow;
    global $typenow;
    if ($pagenow=='edit.php') {
        $filters = get_object_taxonomies($typenow);
        foreach ($filters as $tax_slug) {
            $var = &$query->query_vars[$tax_slug];
            if ( isset($var) ) {
                $term = get_term_by('id',$var,$tax_slug);
                $var = $term->slug;
            }
        }
    }
}


*/
