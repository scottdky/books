<?php

/******************** short codes *************************************/

/**
 * Usage: [link url="about-us"]AboutUs[/link]
 */
add_shortcode( 'link', 'link_shortcode' );
function link_shortcode( $attr, $content = null )
{
    extract( shortcode_atts( array( 'url' => '' ), $attr ) );
    return sprintf('<a href="%s">%s</a>', $attr['url'], $content);
}

/**
 * shortcodes for creative commons licenses
 */
add_shortcode('bccl_license_text_hyperlink', 'bccl_license_text_hyperlink_shortcode');
function bccl_license_text_hyperlink_shortcode() {
  return bccl_get_license_text_hyperlink();
}

add_shortcode('bccl_license_image_hyperlink', 'bccl_license_image_hyperlink_shortcode');
function bccl_license_image_hyperlink_shortcode() {
  return bccl_get_license_image_hyperlink();
}

add_shortcode('bccl_full_html_license', 'bccl_full_html_shortcode');
function bccl_full_html_shortcode() {
  //return bccl_license_block();
  return bccl_get_full_html_license();
}




/**
 * PullQuote - incorporates enclosed text into a pullquote style pullout, i.e. <div class="pullquote">text</div>
 * Usage: [pullquote title="How to ..." [float=left/right]]how to blah...[/pullquote]
 */
add_shortcode( 'pullquote', 'pullquote_shortcode' );
function pullquote_shortcode( $attr, $content = null )
{
    extract( shortcode_atts( array( 'title' => '', 'float' => '' ), $attr ) );

    $floattext = (isset($attr['float']))? (sprintf(' style="float: %s"', $attr['float'])) : '';
    $text = '<div class="pullquote"' . $floattext . '>';
    if (isset($attr['title']))
      $text .= '<div class="pullquote-title">' . $attr['title'] . '</div>';
    $text .= '<div class="pullquote-content">' . $content . '</div>';
    $text .= '</div>';

    return $text;
}


/**
 * section_archive - display archive listing of a given section
 * Usage: [section_archive slug="article"]
 */

add_shortcode( 'section_archive', 'section_archive_shortcode' );
function section_archive_shortcode( $attr, $content = null )
{
    extract( shortcode_atts( array( 'slug' => ''), $attr ) );

    // get taxonomy info - http://codex.wordpress.org/Function_Reference/get_term_by
    $term = get_term_by( 'slug', $attr['slug'], 'section'); 

    //echo '<h1 class="page-title">' . $term->name .'</h1>';

//    global $wp_query;
//    $args = array_merge( $wp_query->query, array( 'taxonomy' => 'section' ) );
    $args = 'section=' . $attr['slug'];
    query_posts($args);
    if (have_posts()) {
      while (have_posts()) {
        the_post();

        // load the custom abstract template - renders the article's abstract
        // ref - http://codex.wordpress.org/Function_Reference/locate_template
        locate_template( array('abstract.php'), true, false ); // WP func to locate the desired template file in the theme hierarchy
      }
    } else { 
?>
		  <div class="no-results">
        <h2>No Results</h2><p>No <?php echo $term->name ?> have been found.</p>
		  </div><!--noResults--> 
<?php
    }
?>		
	<div class="oldernewer">
		<p class="older"><?php next_posts_link('&laquo; Older Entries') ?></p>
		<p class="newer"><?php previous_posts_link('Newer Entries &raquo;') ?></p>
	</div><!--.oldernewer-->
<?php

    wp_reset_query();
}



