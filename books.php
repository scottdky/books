<?php
/*
   Plugin Name: Books
   Plugin URI: http://books.provideyourown.com
   Description: manages writing books using wordpress
   Version: 0.5
   Author: Scott Daniels
   Author URI: http://provideyourown.com
   License: GPL2
*/

// Path constants
define('BOOKS_BASEDIR', dirname(plugin_basename(__FILE__)));
define('BOOKS_LIBDIR', BOOKS_BASEDIR . '/library');

require_once(BOOKS_LIBDIR . '/book.php');
require_once(BOOKS_LIBDIR . '/book_contents.php');
require_once(BOOKS_LIBDIR . '/shortcodes.php');
require_once(BOOKS_LIBDIR . '/tinymce.php');
require_once(BOOKS_LIBDIR . '/book_export/book_export_tool.php');
require_once(BOOKS_LIBDIR . '/book_stats/book_stats_tool.php');
//require_once(BOOKS_LIBDIR . '/admin.php');

// add support for new 3.0 menu scheme
// do we need this?? - add_theme_support( 'menus' );

add_theme_support('editor_style');
add_editor_style('editor-style.css');


// set default post visibility to private instead of public
// ref - http://wordpress.org/support/topic/how-to-set-new-post-visibility-to-private-by-default?replies=14#post-2074408
// ref - http://wordpress.stackexchange.com/questions/20729/easiest-way-to-make-post-private-by-default
add_action( 'post_submitbox_misc_actions' , 'default_post_visibility' );
function default_post_visibility(){
	global $post;

	if ( 'publish' == $post->post_status ) {
		$visibility = 'public';
		$visibility_trans = __('Public');
	} elseif ( !empty( $post->post_password ) ) {
		$visibility = 'password';
		$visibility_trans = __('Password protected');
	} elseif ( $post_type == 'post' && is_sticky( $post->ID ) ) {
		$visibility = 'public';
		$visibility_trans = __('Public, Sticky');
	} else {
		$post->post_password = 'test';
		$visibility = 'password';
		$visibility_trans = __('Password protected');
	} ?>

	<script type="text/javascript">
		(function($){
			try {
				$('#post-visibility-display').text('<?php echo $visibility_trans; ?>
');
				$('#hidden-post-visibility').val('<?php echo $visibility; ?>');
				$('#visibility-radio-<?php echo $visibility; ?>').attr('checked', tr
ue);
			} catch(err){}
		}) (jQuery);
	</script>
	<?php
}

